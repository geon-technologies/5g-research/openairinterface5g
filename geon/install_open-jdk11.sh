#!/bin/sh

# instructions coming from this website: https://openjdk.org/groups/build/doc/building.html

#this script automatially installs openjdk11-jre-headless_11.0.15+10-0ubuntu0epnjdk11-jre-healess_11.0.15+10-0ubuntu0
apt-get update && apt-get install -y -f openjdk-11-jdk libx11-dev libxext-dev libxrender-dev libxrandr-dev libxtst-dev libasound2-dev autoconf zip libcups2-dev
git clone https://github.com/openjdk/jdk11u
cd jdk11u

#configures and makes Openjdk-11-jdk and tests build
bash configure
make images
./build/*/images/jdk/bin/java -version
make run-test-tier1
cd ..


