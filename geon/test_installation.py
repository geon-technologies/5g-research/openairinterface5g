#!/usr/bin/env python

import os

expected_files = [
    "/usr/bin/jvm/java_folder/bin/javac",
    "/usr/lib/jvm/openjdk-11",
    "/usr/lib/aarch64-linux-gnu/liblapacke.a",
    "/usr/lib/aarch64-linux-gnu/liblapack.so",
    "/usr/lib/aarch64-linux-gnu/liblapack.so.3",
    "/usr/bin/python3.6m",
    "usr/bin/python3.6m-config",
    "/usr/bin/python3.6-config",
    "/usr/bin/python3.6",
    "/usr/bin/python3",
    "/usr/lib/python3",
    "/usr/lib/python3.8",
    "/usr/lib/python3.6",
    "/usr/lib/python3.7",
    "/etc/python3.6",
    "/etc/python3",
    "/usr/local/lib/python3 ",
    "/usr/local/lib/python3.6",
    "/usr/include/python3.6m",
    "/usr/include/python3.6",
    "/usr/share/python3",
    "/usr/lib/aarch64-linux-gnu/graphviz ",
    "/usr/share/graphviz",
    "/usr/lib/aarch64-linux-gnu/libncurses.so",
    "/usr/lib/aarch64-linux-gnu/libncurses.a",
    "/usr/bin/apt", 
    "/usr/lib/apt",
    "/etc/apt",
    "/usr/bin/cmake ",
    "/usr/lib/aarch64-linux-gnu/cmake",
    "/usr/local/lib/cmake",
    "/usr/lib/libforms.so" ,
    "/usr/lib/libforms.a",
    "/usr/bin/sudo",
    "/usr/lib/sudo",
    "/etc/subversion",
    "/usr/bin/mono",
    "/usr/lib/mono ",
    "/etc/mono",
    "/usr/share/mono",
    "/usr/lib/aarch64-linux-gnu/libatlas.a",
    "/usr/lib/aarch64-linux-gnu/libatlas.so",
    "/usr/lib/aarch64-linux-gnu/liblapack.a ",
    "/usr/lib/aarch64-linux-gnu/liblapack.so",
    "/usr/bin/flex ",
    "/usr/share/info/flex.info-1.gz ",
    "/usr/share/info/flex.info.gz ",
    "/usr/share/info/flex.info-2.gz",
    "/usr/share/debhelper",
    "/usr/lib/aarch64-linux-gnu/libfontconfig.a ",
    "/usr/lib/aarch64-linux-gnu/libfontconfig.so",
    "/usr/bin/pip3" 
]

for c_file in expected_files:
    assert os.path.isfile(c_file),\
        "Expected %s was not found" % c_file

